// This line includes the Mouse.h library, some code that helps us send HID messages emulating a mouse
#include <Mouse.h>

int pinButton1 = A0;
int pinButton2 = A1;
int pinXAxis   = A2;
int pinYAxis   = A3;

void setup(){
    Serial.begin(115200);

    // This sends a message to your computer, telling it that we're a mouse
    Mouse.begin();

    pinMode(pinButton1, INPUT_PULLUP);
    pinMode(pinButton2, INPUT_PULLUP);
    pinMode(pinXAxis, INPUT_PULLUP);
    pinMode(pinYAxis, INPUT_PULLUP);
    
}

void loop(){
    // Everything in this block only runs if button1 == 0 (button down)
    if (digitalRead(pinButton1) == 0){
        // Clicks the mouse
        Mouse.click();
        Serial.println("CLICK");
    }
    
    delay(10);
}