### Lesson 2 - Cookie Clicker

02_cookie_clicker.ino

In this lesson we will learn how to send mouse clicks from our pickaxe.

This will click so long as the button is down (digitalRead(A0 == 0)

More information on the mouse.h library that we will use can be found [here](https://www.arduino.cc/reference/en/language/functions/usb/mouse/mouseclick/)



If you want it to go faster or slower, try changing the delay(10) between each click.
