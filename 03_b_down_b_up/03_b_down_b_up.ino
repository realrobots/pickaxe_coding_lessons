#include <Mouse.h>

int pinButton1 = A0;
int pinButton2 = A1;
int pinXAxis   = A2;
int pinYAxis   = A3;

// We add it line to remember what the button state is (1=off, 0=on)
int button1State = 1;

void setup(){
    Serial.begin(115200);

    Mouse.begin();

    pinMode(pinButton1, INPUT_PULLUP);
    pinMode(pinButton2, INPUT_PULLUP);
    pinMode(pinXAxis, INPUT_PULLUP);
    pinMode(pinYAxis, INPUT_PULLUP);
    
}

void loop(){
    // If the button was off, but now the button is reading as on, we record it as a press
    if (button1State == 1 && digitalRead(pinButton1) == 0){
        // We remember the new state
        button1State = 0;
        // We tell the computer we pressed the button
        Mouse.press();
        // We send a message to the serial monitor so we can see whats happening
        Serial.println("Press");
    }

    // We do the same thing for when the button was on, but now the button reads as up.
    if (button1State == 0 && digitalRead(pinButton1) == 1){
        button1State = 1;
        Mouse.release();
        Serial.println("Release");
    }
    
    delay(10);
}