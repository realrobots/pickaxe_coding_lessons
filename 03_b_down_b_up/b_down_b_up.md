### Lesson 3 - Button Down, Button Up

03_b_down_b_up.ino

We usually won't want to click as fast as possible, usually we only want the computer to know when we push a button down, and when we release it.

This is slightly tricker to code because we need to remember what our current button state is, and only send a message if it changes.