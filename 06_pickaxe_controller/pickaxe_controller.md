### Lesson 6 - Pickaxe Controller

06_pickaxe_controller.ino

This is the final code for the pickaxe controller, if you've skipped straight to this you'll need to go back to lesson 5 and install the accelerometer library.

##### Set Buttons

To chance what buttons do, find the 'OnButtonAUp()' and 'OnButtonADown()' functions and change teh Keyboard.press() and Keyboard.release() lines.

If you want them to press a mouse button then replace the Keyboard.press() with a Mouse.press() line, same with the release.


##### Set Thumbstick

You can fiddle with the thumbstick settings within the SetThumbstickAxes() block.


##### Set Accelerometer

Right now left mouse button will be presses when you start whipping the pickaxe back and forth, and will be released when you stop.

To change that, find the Mouse.press() and Mouse.release() lines in the UpdateWhipping() block and change them to what you want.