#include "Adafruit_Sensor.h"
#include "Adafruit_ADXL345_U.h"

#include <Mouse.h>
#include <Keyboard.h>

// Set the pins for each input
int pinButton1 = 10;
int pinButton2 = 16;
int pinXAxis   = A0;
int pinYAxis   = A1;

/* Assign a unique ID to this sensor at the same time */
Adafruit_ADXL345_Unified accel = Adafruit_ADXL345_Unified(12345);

// These values remember if we are whipping the pickaxe back and forth and
// also set the minimum values and timeouts to start/stop whipping
int whipThreshold = 40;
int whipTimeout = 200;
long whipStart = -1000;
long lastWhip = -1000;
bool isWhipping = false;

// This structure remembers all our inputs, this iwll make it easier later
struct Inputs
{
  int input_thumb_x = 0;
  int input_thumb_y = 0;
  int input_button_A = 0;
  int input_button_B = 0;
  float x_accel = 0;
  float y_accel = 0;
  float z_accel = 0;
  float roll = 0;
  float pitch = 0;
};

// We define an Inputs object like the one above, to remember our current inputs.
Inputs currentInputs;
// And onother to remember what they were to check for changes.
Inputs previousInputs;

void setup()
{
  Serial.begin(115200);

  pinMode(pinButton1, INPUT_PULLUP);
  pinMode(pinButton2, INPUT_PULLUP);
  pinMode(pinXAxis, INPUT_PULLUP);
  pinMode(pinYAxis, INPUT_PULLUP);

  Mouse.begin();

  // Begin the connection to the accelerometer
  accel.begin();
  accel.setRange(ADXL345_RANGE_16_G);
}

void OnButtonAUp() {
  Serial.println("Button A up");
  Keyboard.release('e'); // Change this to set what happens when button A is pressed
}

void OnButtonADown() {
  Serial.println("Button A Down");
  Keyboard.press('e'); // Need to change this one too
}

void OnButtonBUp() {
  Serial.println("Button B up");
  Keyboard.release(KEY_UP_ARROW); // special keycodes like this can be found at the bottom of this page
}

void OnButtonBDown() {
  Serial.println("Button B Down");
  Keyboard.press(KEY_UP_ARROW);
}

void SetThumbstickAxes() {
  if (currentInputs.input_thumb_x > 512 - 100 && currentInputs.input_thumb_x < 512 + 100) {
    currentInputs.input_thumb_x = 512;
  }
  currentInputs.input_thumb_x = map(currentInputs.input_thumb_x, 0, 1023, -20, 20);

  if (currentInputs.input_thumb_y > 512 - 100 && currentInputs.input_thumb_y < 512 + 100) {
    currentInputs.input_thumb_y = 512;
  }
  currentInputs.input_thumb_y = map(currentInputs.input_thumb_y, 0, 1023, -20, 20);

  Mouse.move(currentInputs.input_thumb_x, currentInputs.input_thumb_y, 0);

  // You can make these inputs press buttons instead. eg.
  // if (currentInputs.inputs_thumb_y > 5){
  //     Keyboard.press(KEY_ARROW_UP);
  // }
}


void loop()
{
  UpdateInputValues();

  UpdateWhipping();

  if (currentInputs.input_button_A != previousInputs.input_button_A) {
    if (currentInputs.input_button_A == 1) {
      OnButtonADown();
    } else if (currentInputs.input_button_A == 0) {
      OnButtonAUp();
    }
  }

  if (currentInputs.input_button_B != previousInputs.input_button_B) {
    if (currentInputs.input_button_B == 1) {
      OnButtonBDown();
    } else if (currentInputs.input_button_B == 0) {
      OnButtonBUp();
    }
  }

  SetThumbstickAxes();

  // Print the current values for testing. (Read these in the Serial PLOTTER, not the monitor)
  //    Serial.print(currentInputs.x_accel);
  //    Serial.print("\t");
  //    Serial.print(currentInputs.y_accel);
  //
  //    Serial.print("\t");
  //    Serial.print(currentInputs.z_accel);
  //    Serial.print("\t");
  //    Serial.println(isWhipping * 120);


  previousInputs = currentInputs;
  delay(10);
}

void UpdateWhipping() {
  // Check to see if the pickaxe is whipping back and forth
  if (currentInputs.y_accel > whipThreshold)
  {
    lastWhip = millis();
    if (!isWhipping)
    {
      Mouse.press();
    }
    isWhipping = true;
  }

  // If the pickaxe is no longer whipping back and forth
  if (millis() - lastWhip > whipTimeout)
  {
    if (isWhipping)
    {
      Mouse.release();
    }
    isWhipping = false;
  }
}

// This function gets the current values from our accelerometer
void UpdateInputValues()
{
  sensors_event_t event;
  accel.getEvent(&event);

  currentInputs.x_accel = event.acceleration.z;
  currentInputs.y_accel = event.acceleration.x;
  currentInputs.z_accel = event.acceleration.y;

  currentInputs.pitch = atan2(currentInputs.y_accel, currentInputs.z_accel) * 57.3;
  currentInputs.roll = atan2((-currentInputs.x_accel), sqrt(currentInputs.y_accel * currentInputs.y_accel + currentInputs.z_accel * currentInputs.z_accel)) * 57.3;

  currentInputs.input_button_A = !digitalRead(pinButton1);
  currentInputs.input_button_A = !digitalRead(pinButton2);
  currentInputs.input_thumb_x = analogRead(pinXAxis);
  currentInputs.input_thumb_y = analogRead(pinYAxis);


}

/*
  Special keycode below, for letter keys type the letter in single quotes
  eg. Keyboard.press('h');
  Keyboard.release('h');

  To use the special keys
  Keyboard.press(KEY_UP_ARROW);
  Keyboard.release(KEY_UP_ARROW);

  KEY_LEFT_CTRL
  KEY_LEFT_SHIFT
  KEY_LEFT_ALT
  KEY_LEFT_GUI
  KEY_RIGHT_CTRL
  KEY_RIGHT_SHIFT
  KEY_RIGHT_ALT
  KEY_RIGHT_GUI
  KEY_UP_ARROW
  KEY_DOWN_ARROW
  KEY_LEFT_ARROW
  KEY_RIGHT_ARROW
  KEY_BACKSPACE
  KEY_TAB
  KEY_RETURN
  KEY_ESC
  KEY_INSERT
  KEY_DELETE
  KEY_PAGE_UP
  KEY_PAGE_DOWN
  KEY_HOME
  KEY_END
  KEY_CAPS_LOCK
  KEY_F1
  KEY_F2
  KEY_F3
  KEY_F4
  KEY_F5
  KEY_F6
  KEY_F7
  KEY_F8
  KEY_F9
  KEY_F10
  KEY_F11
  KEY_F12
  KEY_F13
  KEY_F14
  KEY_F15
  KEY_F16
  KEY_F17
  KEY_F18
  KEY_F19
  KEY_F20
  KEY_F21
  KEY_F22
  KEY_F23
  KEY_F24
*/