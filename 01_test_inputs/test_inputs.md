### Lesson 1 - Test Inputs

01_test_inputs.ino

This file contains the code to 

1. Define the pins for each input (buttons and thumbstick axes)
2. Begin a Serial connection to send data from the device to the computer
3. Check each input and send the result through the Serial connection

Open the file '01_test_inputs.ino' in the Arduino IDE, make sure the correct board and port are selected, then press the 'Upload' button.

Once the code has uploaded you can use the 'Serial Monitor' to see if the correct data is coming.

The serial monitor can be opened in the 'Tools menu', Tools->Serial Monitor.

![arduino_ide](./serial_monitor.png)
