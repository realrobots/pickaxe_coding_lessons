
// These lines record our pin numbers with a keyword so we don't need to remember the numbers
int pinButton1 = A0;
int pinButton2 = A1;
int pinXAxis   = A2;
int pinYAxis   = A3;

// Everything in the setup(){} block runs one time when the device starts up 
void setup(){
    // This line begins Serial communication through the USB cable
    Serial.begin(115200);

    // These lines set our pins into the correct INPUT mode so we can read them
    pinMode(pinButton1, INPUT_PULLUP);
    pinMode(pinButton2, INPUT_PULLUP);
    pinMode(pinXAxis, INPUT_PULLUP);
    pinMode(pinYAxis, INPUT_PULLUP);
    
}

void loop(){
    // This line prints data through the Serial line
    // The data we are sending is a read of the button
    // digital means 1(ON) or 0(OFF)
    Serial.print(digitalRead(pinButton1));
    Serial.print(" ");
    Serial.print(digitalRead(pinButton2));
    Serial.print(" ");
    // analogRead gives us a value from 0-1023. 0=0v, 1023=5v
    Serial.print(analogRead(pinXAxis));
    Serial.print(" ");
    Serial.print(analogRead(pinYAxis));
    // This special character \n indicates a line break, like pressing 'enter'
    Serial.print("\n");

    // A small, 10millisecond delay prevents too much data swamping the serial link
    delay(10);
}