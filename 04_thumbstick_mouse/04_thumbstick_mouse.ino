#include <Mouse.h>

int pinButton1 = A0;
int pinButton2 = A1;
int pinXAxis   = A2;
int pinYAxis   = A3;


int xAxisValue = 512;
int yAXisValue = 512;

void setup(){
    Serial.begin(115200);

    Mouse.begin();

    pinMode(pinButton1, INPUT_PULLUP);
    pinMode(pinButton2, INPUT_PULLUP);
    pinMode(pinXAxis, INPUT_PULLUP);
    pinMode(pinYAxis, INPUT_PULLUP);
    
    delay(5000);
}

void loop(){
    xAxisValue = analogRead(pinXAxis);
    if (xAxisValue > 512-100 && xAxisValue < 512+100){
        xAxisValue = 512;
    }
    xAxisValue = map(xAxisValue, 0, 1023, -20, 20);

    Mouse.move(xAxisValue, 0, 0);
    
    delay(10);
}