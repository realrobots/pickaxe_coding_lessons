### Lesson 4 - Thumbstick Mouse

04_thumbstick_mouse.ino

The thumbstick movements are a little different, rather than a digital read of 1 or 0, we read a value from 0-1023.

The tricky thing here is that we need to calibrate our values, yours might be different to mine.

We will need to make a variable to hold the axis value

`int xAxisValue = 512;`

In our loop we will start by recording the current value.

`xAxisValue = analogRead(pinXAxis);`

The tricky thing is that this gives us a value from 0-1023, but we want to move the mouse by pixel, and we want to go negative as well as positive.

We want a range of -30 <-> 30 so that we can go left and right. We do this with the `map(value, lowest, highest, newLowest, newHighest)` function.



`xAxisValue = map(xAxisValue, 0, 1023, -20, 20);`

and finally we need to send the message to move the mouse.

`Mouse.move(xAxisValue, 0, 0);`

The second number is for the yAxis, the third is for a mouse wheel if you have one.



The final thing is that the thumbstick rarely sits at exactly 512 so the mouse will drift a bit, we can compensate for this with a 'deadzone'

`if (xAxisValue > 512-100 && xAxisValue < 512 + 100){`

​	`XAxisValue = 512;`

`}`

This makes sure that any value that is within 100 of 512, will be locked to 512.



**TROUBLESHOOTING**

If you are getting totally weird results, your thumbstick might need more calibrating. Do a Serial.println(xAxisValue) right after your do the analog read and find out what the middle, left and right values are and replace those in the map() function with those.

