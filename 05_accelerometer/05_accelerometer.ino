#include "Adafruit_Sensor.h"
#include "Adafruit_ADXL345_U.h"

#include <Mouse.h>

/* Assign a unique ID to this sensor at the same time */
Adafruit_ADXL345_Unified accel = Adafruit_ADXL345_Unified(12345);

// These values remember if we are whipping the pickaxe back and forth and
// also set the minimum values and timeouts to start/stop whipping
int whipThreshold = 40;
int whipTimeout = 200;
long whipStart = -1000;
long lastWhip = -1000;
bool isWhipping = false;

// This structure remembers all our inputs, this iwll make it easier later
struct Inputs
{
    int input_thumb_x = 0;
    int input_thumb_y = 0;
    int input_button_A = 0;
    int input_button_B = 0;
    float x_accel = 0;
    float y_accel = 0;
    float z_accel = 0;
    float roll = 0;
    float pitch = 0;
};

// We define an Inputs object like the one above, to remember our current inputs.
Inputs currentInputs;
// And onother to remember what they were to check for changes.
Inputs previousInputs;

void setup()
{
    Serial.begin(115200);

    Mouse.begin();

    // Begin the connection to the accelerometer
    accel.begin();
    accel.setRange(ADXL345_RANGE_16_G);
}

void loop()
{
    UpdateInputValues();

    // Check to see if the pickaxe is whipping back and forth
    if (currentInputs.y_accel > whipThreshold)
    {
        lastWhip = millis();
        if (!isWhipping)
        {
            Mouse.press();
        }
        isWhipping = true;
    }

    // If the pickaxe is no longer whipping back and forth
    if (millis() - lastWhip > whipTimeout)
    {
        if (isWhipping)
        {
            Mouse.release();
        }
        isWhipping = false;
    }

    // Print the current values for testing. (Read these in the Serial PLOTTER, not the monitor)
    Serial.print(currentInputs.x_accel);
    Serial.print("\t");
    Serial.print(currentInputs.y_accel);

    Serial.print("\t");
    Serial.print(currentInputs.z_accel);
    Serial.print("\t");
    Serial.println(isWhipping * 120);
    delay(10);
}

// This function gets the current values from our accelerometer
void UpdateInputValues()
{
    sensors_event_t event;
    accel.getEvent(&event);

    currentInputs.x_accel = event.acceleration.z;
    currentInputs.y_accel = event.acceleration.x;
    currentInputs.z_accel = event.acceleration.y;

    currentInputs.pitch = atan2(currentInputs.y_accel, currentInputs.z_accel) * 57.3;
    currentInputs.roll = atan2((-currentInputs.x_accel), sqrt(currentInputs.y_accel * currentInputs.y_accel + currentInputs.z_accel * currentInputs.z_accel)) * 57.3;
}