### Lesson 5 - Accelerometer

05_accelerometer.ino

#### Install the library

To use the accelerometer we need to use another library, but this one doesn't come included with the Arduino IDE like the Mouse.h library does.

To download the new library we need to go to Sketch->Include Library->Manage Libraries

Search for 'Adafruit adxl345' find that library in the list and press "INSTALL"

It will as if you want to also download the prerequisites, definitely say YES to this.



#### The Code

There's a lot here, the library grabs the current x, y and z acceleration values from the accelerometer we put in our pickaxe. We then do some math to convert those into pitch and roll values that we can use for mouse motion if we want.

We also check the acceleration on the forward-backward axis and if it's over a certain amount we can use it as a button press.

Take the time to read the code, as I've put comments where you can change the button presses.

While testing this, it's best to use the Serial Plotter rather than the Serial Monitor.



![](./whipping.png)

This graph in the serial plotter shows the red peaks as the pickaxe is whipped back and forth. You can see the yellow square wave indicating when we recognize the whipping is happening and trigger a button press.

The button press even happens when the yellow line goes up, and button release is triggered when the yellow bar goes down.
