## Pickaxe Coding Lessons

This series of lessons will help teach you to code your pickaxe game controller. The same theory can be used to make any game controller or keyboard/mouse emulator you might think of.

Download this entire file and you can find each of the following lessons in their own folder.

Lesson 1 - Reading Inputs

Lesson 2 - Cookie Clicker

Lesson 3 - Button Down, Button Up

Lesson 4 - Thumbstick Mouse

Lesson 5 - Accelerometer

Lesson 6 - Pickaxe Controller (Final code)

### Install Software

The software used to write and upload code to the device is called the 'Arduino IDE'

We need to begin by downloading that, make sure you get the correct version for your computer, ie Mac or Windows.

[Arduino Download Website](https://www.arduino.cc/en/software)

As you install the Arduino IDE, make sure to allow driver permissions as these are needed to access the USB and communicate with the pickaxe.

When you've installed and opened the software it should look like this.

![arduino_ide](./arduino_ide.png)

This is empty code, it has a 'setup(){}' block which is where all our code that will run one time at startup will go, and a 'loop(){}' block which will repeat while the device has power.

Before we begin lessons we need to make sure our software can successfully upload code. To do this we need to select two important options from the TOOLS menu at the top of the page.

**Select the correct board**

Tools->Board->Arduino Micro

**Select the correct port**

Tools->Port-> (The correct port may vary)

If you don't see anything listed under ports, or can only see ports that aren't the Arduino Micro in the pickaxe, then there is likely a bad connection or the Arduino IDE doesn't have proper permissions to access the USB.



**Upload the code**

When those are selected you can press the 'Upload' button at the top of the screen. This is the small triangle button near the top-left.

When you press that you should get a 'Compiling' message at the bottom of the screen as the code is converted into something the device can understand, then it will read 'Uploading...' as the code is sent. Finally it should read 'Done uploading', indicating a successful upload.



**Troubleshooting**

This step occasionally fails, if it does try uploading again, if it still fails unplug and replug the usb and try again. If it still fails, check the port selection.



